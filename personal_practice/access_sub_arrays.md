```ruby
def access_sub_arrays(param)
  results = []
  start = 0
  length = param.length - 1
  start.upto(length) do |idx1| 
    (idx1 + 1).upto(length) do |idx2| 
      subarr = param[idx1..idx2]
      results << subarr
    end
  end
  results
end

local_arr = (1..5).to_a
p access_sub_arrays(local_arr) == 

```

- line 1: Define a new method that takes an array as an argument
- line 2: Initialize a container to use for the method's return
- line 3: Declare which index number to begin iteration at
- line 4: Declare which end point of iteration to stop at
- line 5: Top-level iteration through given range, in this case 0 to -1 from the argument's length, 'idx1' represents the current number of the range in this iteration
- line 6: Second-level iteration through given range, in this case from 1 (`idx1 + 1`) to -1 from argument's length, `idx2` represents the current number of the range in this iteration.
- line 7: we now use the range of numbers saved as local variables in order to pass the range of indices from the given array. In this case, 0 to 1 in the first iteration, 0 to 2 in the second, ...
- line 8: Append each subarray to the results array in iteration
- line 9: End second-level iteration, start from top-level iteration until the 'length' argument has been met
- line 11:  Implicit return for the method

------------------------------------------------------------------------------

- nice to have a second level start value to make it more user friendly