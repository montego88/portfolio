

```ruby
=begin
--------------------------------------------------------------
Problem:
Create a method that takes an array, and returns a new array made up of each contiguous subarray from the original. Each subarray should be a minimum length of 2.

You may not use the #upto, #downto, or #each or #range methods, only use a brute force method to achieve the results

Examples:
sub_arrays([1, 2, 3, 4]) == [[1, 2], [1, 2, 3], [1, 2, 3, 4], [2, 3], [2, 3, 4], [3, 4]]


--------------------------------------------------------------

# BREAKDOWN: 
- input: array
- output: nested array
create subarrays from each index of a given array starting from the current index until the end of the array.

## DATA
- output array is a one level nested array [[1, 2], [1, 2, 3]....]
- each iteration needs to append the values as an array to the top-level array

## APPROACH: top-level iteration accesses current value from index 0 and save as local variable `idx1`. starting with idx1 append `idx1` and the next item (idx1 + 1) to the output array as a subarray. On next iteration, increase the + 1 to include the next index item after including all those before.

- assign a return container
- assign `idx1 = 0` for the top-level loop
- create top-level iteration that increases idx1 by 1
  - access array[idx1] as starting value for range
  - create an second level loop that increases the range by 1
    - assign `inner_counter` starting at 1
    - `idx2` = `idx1 + inner_counter`
    - append the values from array[idx1..idx2] to the output array
  - exit loop when idx2 == array.size
- exit top-level loop when idx1 == array.size
		
## PSEUDOCODE:
def sub_arrays(arr)
	initialize local variable `results` to empty array
	initialize local variable `idx1` to 0 as counter for current index of array
  create a local variable `counter_mod` initialized to `1` as secondary counter for second level iteration
	top-level iteration --> each element of array
    initialize `idx2` to the value of `idx1 + counter_mod`
		second-level iteration --> each element of array starting from idx1
			append `arr[idx1..idx2]` to `results` array
			increment `counter_mod` by `1`
			exit the second-level iteration when `counter_mod` is equal to `arr` length
    end
		`idx1` increment by `1`
		exit the top-level iteration when `idx1 == arr length - 1`
  end
end
## IMPLEMENTAION NOTES: Was still a little fuzzy about break points with both top-level and second-level loops as well how to control the return values. It's best to start small and scale up instead of evaluating larger groups.
=end

def brute_sub_arrays(arr)
  results = []
  idx1 = 0
  counter = 1
  loop do
    idx2 = idx1 + counter
    loop do 
      results << arr[idx1..idx2]
      idx2 += 1
      break if idx2 == arr.size
    end
    idx1 += 1
    break if idx1 == arr.size - 1
  end
  results
end

local_arr = (1..4).to_a #=> [1, 2, 3, 4]
p brute_sub_arrays(local_arr)

```