=begin

PROBLEM:
Given an integer, convert the integer into an array. If the number is a negative integer, begin array with '-'. Provide a solution that uses a mathematical approach based on calculations instead of a method that will convert the integer into an array.


Examples:
convert_to_array(12345) == [1, 2, 3, 4, 5]
convert_to_array(-12345) == ['-', 1, 2, 3, 4, 5]

--------------------------------------------------------------

BREAKDOWN: return an array from a given integer and assume that the integer is a valid integer. 
- return array is made up of integers
  - if number is negative, prepend the return array with a string
  - each digit of given integer is provided in the array as the same order


DATA STRUCTURES:
Input: integer
Output: array of integers, or mixed string and integers


APPROACH: 
- create a method that takes an integer: `convert_to_array(int)`
- get count of digits that make up the given integer - 1: `outer_counter`
- create a container object for method to return as empty array: `results`
- create an alias to `int`: `remainder`
- iterate `outer_counter` times and perform calculation on `int`
  - create an integer to use for division with `int`: `number` 
    - number should be `10 ** outer_counter`
  - append quotient to `results` array
  - reassign `remainder` of calculation
  - decrement internal_counter by 1
- return array
  - [1, 2, 3, 4]
  - if `int` is negative prepend return array with '-' string element


		
PSEUDOCODE:
def convert_to_array(int)
  initialize local variable `outer_count` to return value getting the integers length of digits
  initialize local variable `results` to empty array
  initialize local variable `remainder` to `int`
  begin iteration `outer_counter` amount of times
    initialize local variable `number` to the return value of (10 ** outer_counter) #THIS MAY NEED TO BE A DIFFERENT VARIABLE#
    divide `int` by `number`
      - assign local variable `quotient` to quotient of calculation
        - append `quotient` to `results` array.
      - reassign `remainder` variable scoped outside of the block to the remainder of the calculation.
    - decrement the `outer_counter` by 1 #THIS MAY NEED TO BE A DIFFERENT VARIABLE#
  return result
end

IMPLEMENTATION/NOTES:
Although this solution works and provides a possibility to work with, I'd like
to find an alternative way to find the length of an integer in terms of the
digits that make up the integer itself. 

currently there is also the issue of having to manually check if the outer_counter is at 0 in order to manually append the last digit. Again, this works but it would be far better to find the correct way to target the last integer.

=end

def signed_prefix(int)
  results = '+'
  if int.negative?
    results = '-'
  end 
  results
end

def signed_prefix_negative(int)
  results = '-'
  if int.negative?
    results 
  end 
end

def if_negative(int)
  if int.negative?
    (int *= -1)
  else
    int
  end
end

def convert_to_array(int)
  results = []
  signed = signed_prefix(int)
  int = if_negative(int)
  outer_counter = int.digits.size - 1
  number = int
  outer_counter.times do 
    div = 10 ** outer_counter
    quotient, remainder = number.divmod(div)
    results << quotient
    outer_counter -= 1
    if outer_counter == 0
      results << remainder
    else
      number = remainder
    end
  end
  if signed == '-'
    results.prepend(signed)
  end
  results
end 

p convert_to_array(-1234) ==  ['-', 1, 2, 3, 4]
p convert_to_array(1234) ==  [1, 2, 3, 4]
