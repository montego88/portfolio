# Problem
# Write a method that determines and returns the ASCII string value of a string
# that is passed in as an argument. The ASCII string value is the sum of the
# ASCII values of every character in the string. (You may use String#ord to
# determine the ASCII value of a character.)
#
# Explicit:
# - take a string as input
# - determine the ASCII string value of the input
# - return the ASCII string value
#   - the ASCII string value is the sum of every character's ASCII value in the string
#   - String#ord can determine the ASCII value
#
# Implicit:
#
# Input: string
# Output: integer
#
# Examples:
# ascii_value('Four score') == 984
# ascii_value('Launch School') == 1251
# ascii_value('a') == 97
# ascii_value('') == 0
#
# Data Structure:
# - string collection contains spaces
# - string may be empty
# - output must be an integer
#   - integer is the sum of each character's ASCII numerical representation
#
# Algorithm:
# 1. take the input string
# 2. get ASCII value of each character
# 3. assign/increment ASCII numerical value of character to a local variable (`sum`) 
# 4. repeat steps 2-3 until iteration through string is complete
# 5. return `sum`
#
# Code Implementation:

def ascii_value(str)
  sum = 0
  str.chars.each do |char|
    sum += char.ord
  end
  sum
end

p ascii_value('Four score') == 984
p ascii_value('Launch School') == 1251
p ascii_value('a') == 97
p ascii_value('') == 0
