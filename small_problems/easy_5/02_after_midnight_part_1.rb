# PROBLEM
# The time of day can be represented as the number of minutes before or after
# midnight. If the number of minutes is positive, the time is after midnight.
# If the number of minutes is negative, the time is before midnight.

# Write a method that takes a time using this minute\-based format and returns
# the time of day in 24 hour format (hh:mm). Your method should work with any
# integer input.
# 
# You may not use ruby's `Date` and `Time` classes.
#
# Explicit:
# - Input: integer
# - Output: string (hh:mm) format.
# - take input integer representing minute-based format
# - return time of day in 24 hour format (hh:mm)
# - disregard DST and Standard time
#
# Implicit:
# - if integer is positive, the time is after midnight
# - in integer is negative, the time is before midnight
#
# Examples:
# p time_of_day(0) == "00:00"
# p time_of_day(-3) == "23:57"
# p time_of_day(35) == "00:35"
# p time_of_day(-1437) == "00:03"
# p time_of_day(3000) == "02:00"
# p time_of_day(800) == "13:20"
# p time_of_day(-4231) == "01:29"
#
# Data Structure:
# Input is an integer that we need to convert into a 24 hour format
# - for example: if the integer is 60, that equates to 60 minutes past midnight
# which would need to be formatted as (01:00) or '90' would be (01:30) ...
#
# Output is a string representation of the 24 hour format. 
# So it will need to have zeros padding the single integers of 0..9
#
# Algorithm:
# **positive integers up to 1440**
# - Create variable to hold result of calculation
#   - 'twentyfour_time'
#
# - Create variables to hold the individual hour/minute return values from calculations
#   - 'hh'
#   - 'mm'
#
# - CALCULATIONS:
# - divide the input by 60
#   - append result to 'hh'
#   - if there is a remainder
#     - append result to 'mm'
#
# - OUTPUT: 
# - output string with padded 0s for both 'hh' and 'mm'
#
# **Problem: minutes > 24 hours
# - return correct hour
# - create an offset to adjust the 'twentyfour_time' for when the minutes are greater or equal to 24 hours
# - probably best to have this in a helper method
#   - we can get the offset value by dividing the 'twentyfour_time[0]' integer by 24. The result should then be multiplied by 24 and the amount is what needs to be subtracted from the result of higher_than_1440.divmod(60)
#
# **Problem: Negative Times**
# - if we follow the minutes being greater than 24 hours, we can test similarly to see if the offset value is a negative integer
# - if offset > 1
#   - then reassign offset to offset * 24
#   - subtract offset from given integer argument
# - if offset < 0
#   - then reassign offset to offset * -24
#   - subtract the return value of 'twentyfour_time[0]' by offset
#
# Code Implementation:
# 1440 minutes in a day
#
# For formating the output string with the correct amount of digits it might be better to pass them to a helper method. Regardless the usage we need for this case is: `sprintf("%02d", 'hh')` and `sprintf("%02d", 'mm')` respectively
#
#
# Debug:
#
# 
def get_positive_hour(int)
  offset = int / 24
  hour = 0
  if offset > 1
    offset = offset * 24
    hour = (int - offset)
  elsif offset < 0
    p offset = offset * -24
    p hour = (int + offset)
  else
    hour = int
  end
  sprintf("%02d", hour) 
end

def time_of_day(int)
  hh = 0
  mm = 0
  twentyfour_time = int.divmod(60)
  hh = get_positive_hour(twentyfour_time[0])
  mm = twentyfour_time[1]
  delimeter = ':'
  hh + delimeter + sprintf("%02d", mm)
end

p time_of_day(0) == "00:00"
p time_of_day(-3) == "23:57"
p time_of_day(35) == "00:35"
p time_of_day(-1437) == "00:03"
p time_of_day(3000) == "02:00"
p time_of_day(800) == "13:20"
p time_of_day(-4231) == "01:29"

