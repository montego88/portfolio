=begin
------------------------------------------------------------------------------
Write a method that returns true if its integer argument is palindromic, false
otherwise. A palindromic number reads the same forwards and backwards.

EXAMPLES:
palindromic_number?(34543) == true
palindromic_number?(123210) == false
palindromic_number?(22) == true
palindromic_number?(5) == true


BREAKDOWN:
- define a method that takes an integer
- evaluate if integer is the same in reverse to the original argument


------------------------------------------------------------------------------

APPROACH (high level):
- create a method (`palindromic_number?(int)`) 
- convert `int` to a string
- test if string is the same forwards and backwards

TESTS:
integer to string # => "int"

PSEUDOCODE:
def palindromic_number?(int)
  initialize local variable to return value of integer into string (`string_representation`) 
  test if string_representation is same if reversed
end
=end

def palindromic_number?(int)
  p string_representation = int.to_s
  string_representation == string_representation.reverse
end

p palindromic_number?(34543) == true
#p palindromic_number?(123210) == false
#p palindromic_number?(22) == true
#p palindromic_number?(5) == true

