=begin
------------------------------------------------------------------------------
Write another method that returns true if the string passed as an argument is a
palindrome, false otherwise. This time, however, your method should be
case\-insensitive, and it should ignore all non\-alphanumeric characters. If
you wish, you may simplify things by calling the `palindrome?` method you wrote
in the previous exercise.

EXAMPLES:
real_palindrome?('madam') == true
real_palindrome?('Madam') == true           # (case does not matter)
real_palindrome?("Madam, I'm Adam") == true # (only alphanumerics matter)
real_palindrome?('356653') == true
real_palindrome?('356a653') == true
real_palindrome?('123ab321') == false

BREAKDOWN:
- method takes a string
  - return true if string is a palindrome
  - return false if not
- case does not matter
- only alphanumeric matter


------------------------------------------------------------------------------

APPROACH (high level):
- define a method that takes a string (`real_palindrome?(str)`) 
- save local variable `str` as a temporary object to work with (`worker_string`) this should be assigned by the return value of stripping away all non-alphnumeric characters
- make sure `worker_string` is all same case
- test worker_string is palindrome 
  - `worker_string == worker_string in reverse`
  - return true or false

TESTS:

PSEUDOCODE:
real_palindrome?(str)
  - initialize local variable `worker_string` to return value of stripping away non-alphanumeric elements from string
  - modify the worker_string by changing all elements to same case
  - pass to `palindrome?(str)` helper method
    - returns true or false
=end

def real_palindrome?(str)
  worker_string = str.gsub(/[^a-zA-Z0-9]/, '')
  worker_string.downcase!
  worker_string == worker_string.reverse
end

p real_palindrome?('madam') == true
p real_palindrome?('Madam') == true           # (case does not matter)
p real_palindrome?("Madam, I'm Adam") == true # (only alphanumerics matter)
p real_palindrome?('356653') == true
p real_palindrome?('356a653') == true
p real_palindrome?('123ab321') == false

