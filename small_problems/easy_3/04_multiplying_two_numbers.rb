=begin
------------------------------------------------------------------------------
Create a method that takes two arguments, multiplies them together, and returns
the result.

EXAMPLES:
multiply(5, 3) == 15

BREAKDOWN:
- create a method
- takes two arguments
  - integer and floats only?
- return product of two arguments

------------------------------------------------------------------------------

APPROACH (high level):
- define a method `multiply(x, y)`
- return product of x * y


TESTS:

PSEUDOCODE:
def multiply(x, y)
  - x * y
end

=end
def multiply(x, y)
  x * y
end

p multiply(5, 3) == 15
