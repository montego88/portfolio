=begin
------------------------------------------------------------------------------
Write a program that prompts the user for two positive integers, and then
prints the results of the following operations on those two numbers: addition,
subtraction, product, quotient, remainder, and power. Do not worry about
validating the input.

EXAMPLES:
==> Enter the first number:
23
==> Enter the second number:
17
==> 23 + 17 = 40
==> 23 - 17 = 6
==> 23 * 17 = 391
==> 23 / 17 = 1
==> 23 % 17 = 6
==> 23 ** 17 = 141050039560662968926103

BREAKDOWN:
- method takes two positive integers
- outputs results:
  - addition
  - subtraction
  - product
  - quotient
  - remainder
  - power

------------------------------------------------------------------------------

APPROACH (high level):
- define prompt for syntax
- ask for user input
- define math_processes method
- prompt user for first number
- save first number as var
- prompt user for second number
- save second number as var
- use prompt to return value from each operation

TESTS:
irb: test `#reduce` with all symbols

PSEUDOCODE:
def calculations
  prompt user for integer
  initialize local variable and assign user input (`x`) 
  prompt user for integer
  initialize locla variable and assigne user input (`y`) 
  
  output `x + y` = `result` 
  output `x - y` = `result` 
  output `x * y` = `result` 
  output `x / y` = `result` 
  output `x % y` = `result` 
  output `x ** y` = `result` 

=end

def calculations
  puts "==> Enter the first number:"
  x = gets.chomp.to_i
  puts "==> Enter the second number:"
  y = gets.chomp.to_i

  puts "==> #{x} + #{y} = #{x + y}"
  puts "==> #{x} - #{y} = #{x - y}"
  puts "==> #{x} * #{y} = #{x * y}"
  puts "==> #{x} / #{y} = #{x / y}"
  puts "==> #{x} % #{y} = #{x % y}"
  puts "==> #{x} ** #{y} = #{x ** y}"
end
calculations
