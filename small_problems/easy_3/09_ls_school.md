This is an important exercise in learning a bit more about regex

tags: #count, #delete, #regex

Solution


```ruby
def real_palindrome?(string)
  string = string.downcase.delete('^a-z0-9')
  palindrome?(string)
end
```

#### Discussion

Chances are you reached for a `String#gsub` here to eliminate the non\-alphanumeric characters. There's nothing wrong with that, but we'll take the opportunity to use and talk about `String#delete` instead. `#delete` is an interesting method that takes arguments that sort of look like regular expressions, and then deletes everything formed by the intersection of all its arguments. See the documentation for complete details.

For our purposes, we need to remove the non\-alphanumeric characters; to do that, we tell `delete` to delete everything that isn't a letter or digit. We then pass the result to our original `palindrome?` method to determine if the cleaned up string is a palindrome.

### Further Exploration

Read the documentation for `String#delete`, and the closely related `String#count` (you need to read `count` to get all of the information you need for `delete`.)
