=begin
------------------------------------------------------------------------------
Write a method that returns an Array that contains every other element of an
`Array` that is passed in as an argument. The values in the returned list
should be those values that are in the 1st, 3rd, 5th, and so on elements of the
argument Array.

EXAMPLES:
oddities([2, 3, 4, 5, 6]) == [2, 4, 6]
oddities([1, 2, 3, 4, 5, 6]) == [1, 3, 5]
oddities(['abc', 'def']) == ['abc']
oddities([123]) == [123]
oddities([]) == []

BREAKDOWN:
- create a method that takes an array as an argument
- return an array of every other element in original array indices (0, 2, 4, etc)
- only arrays?
- nested arrays only get top level selections?
------------------------------------------------------------------------------

APPROACH (high level):
- take give array (`arr`) and select odd indices
- return to new array

TESTS:

PSEUDOCODE:
def oddities(arr)
  assign local variable to point to emtpy array for results (`results`) 
  iterate through each element of `arr`
  if the array index is even then append to new array
  return new array
end

=end

# Original Solution
def oddities(arr)
  results = []
  arr.each_with_index do |element, idx|
    results << element if idx.even?
  end
  results
end

# Simple Solution
#def oddities(arr)
#  results = []
#  index = 0
#  while index < arr.size
#    results << arr[index]
#    index += 2
#  end
#  results
#end

p oddities([2, 3, 4, 5, 6]) == [2, 4, 6]
p oddities([1, 2, 3, 4, 5, 6]) == [1, 3, 5]
p oddities(['abc', 'def']) == ['abc']
p oddities([123]) == [123]
p oddities([]) == []

