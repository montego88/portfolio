=begin
------------------------------------------------------------------------------
Write a program that will ask a user for an input of a word or multiple words
and give back the number of characters. Spaces should not be counted as a
character.

EXAMPLES:
input: Please write word or multiple words: walk
output: There are 4 characters in "walk".

input: Please write word or multiple words: walk, don't run
output: There are 13 characters in "walk, don't run".


BREAKDOWN:
- ask user for input of a word or multiple words
- count number of charcters
  - spaces do not count as characters 
- output string with number of characters in "word or multiple words"

------------------------------------------------------------------------------

APPROACH (high level):
- ask user input
- save user input as local variable (`user_input`) 
- count characters of saved user input (`count`) 
- output string with (`count`) and quoted `user_input`

- convert the string to an array
  - don't include spaces
  - count the array length
TESTS:

PSEUDOCODE:
- output: ask user for input # => string
- initialize: local variable `user_input` to return value from user input
- initialize: local variable to return value of converted string to array (skip spaces) (`valid_characters`) 
- initialize: local variable `count` for return value of counting the lenght of the array
- output: There are `count` characters in "`user_input`".
=end

puts "Please write word or multiple words:"
user_input = gets.chomp
valid_characters = user_input.chars
valid_characters.delete_if {|element| element == ' ' }
count = valid_characters.size
puts "There are #{count} characters in \"#{user_input}\"."
