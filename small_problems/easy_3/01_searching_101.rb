=begin
------------------------------------------------------------------------------
Write a program that solicits 6 numbers from the user, then prints a message
that describes whether or not the 6th number appears amongst the first 5
numbers.

EXAMPLES:
==> Enter the 1st number:
25
==> Enter the 2nd number:
15
==> Enter the 3rd number:
20
==> Enter the 4th number:
17
==> Enter the 5th number:
23
==> Enter the last number:
17
The number 17 appears in [25, 15, 20, 17, 23].


==> Enter the 1st number:
25
==> Enter the 2nd number:
15
==> Enter the 3rd number:
20
==> Enter the 4th number:
17
==> Enter the 5th number:
23
==> Enter the last number:
18
The number 18 does not appear in [25, 15, 20, 17, 23].

BREAKDOWN:
- get 6 numbers from user
- inspect if last number appears in first 5 numbers
- output message
- validation of integer?
- takes only integers? What about floats?
------------------------------------------------------------------------------

APPROACH (high level):
- create a prompt for user input (syntax specific)
  - define method
- define a method `searching`
  - create a container to hold the values of user input
  - prompt user for input
    - hardcode the string within the `prompt` method and pass 1st..5th as argument
  - append the input to the container
  - assign variable to represent last item in container
  - iterate through the container and compare if element is equal to variable representing the last element
  if element exists in first 5 numbers
    output "The number `num` appears in `container`
  otherwise
    output "The number `num` does not appaer in `container`

TESTS:

PSEUDOCODE:
def prompt(str)
  if str == '6th'
    str = 'last'
  end conditional
  output "Enter the `str` number"
end

def searching
  initialize a local variable to point to an empty array (`results`) 
  prompt user for input (iteration?)
  append user input to `results`
  iterate through `results` and evaluate if `results` last item appears in first 5 items
  if true
    output string 1
  else
    output string 2
  end
end
=end

def prompt(str)
  puts "==> Enter the #{str} number:"
end

def searching
  results = []
  prompt "1st"
  results << gets.chomp.to_i
  prompt "2nd"
  results << gets.chomp.to_i
  prompt "3rd"
  results << gets.chomp.to_i
  prompt "4th"
  results << gets.chomp.to_i
  prompt "5th"
  results << gets.chomp.to_i
  prompt "last"
  results << gets.chomp.to_i
  last_item = results.last
  first_five = results[0..4].to_a
  if first_five.include?(last_item) 
    puts "The number #{last_item} appears in #{first_five}."
  else
    puts "The number #{last_item} does not appear in #{first_five}."
  end
end
p searching
