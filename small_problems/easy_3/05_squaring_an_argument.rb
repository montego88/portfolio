=begin
------------------------------------------------------------------------------
Using the `multiply` method from the "Multiplying Two Numbers" problem, write a
method that computes the square of its argument (the square is the result of
multiplying a number by itself).

EXAMPLES:
square(5) == 25
square(-8) == 64

BREAKDOWN:
- create a method that takes one argument
- return the square of argument ( arg ** arg)

------------------------------------------------------------------------------

APPROACH (high level):
- create a method that takes one argument
- return the value of the argument multiplied by itself

TESTS:

PSEUDOCODE:
def square(int)
  int * int
end

=end
def square(int)
  int * int
end

p square(5) == 25
p square(-8) == 64
