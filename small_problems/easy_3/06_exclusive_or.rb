=begin
------------------------------------------------------------------------------
The `||` operator returns a truthy value if either or both of its operands are
truthy. If both operands are falsey, it returns a falsey value. The `&&`
operator returns a truthy value if both of its operands are truthy, and a
falsey value if either operand is falsey. This works great until you need only
one of two conditions to be truthy, the so\-called **exclusive or**.

In this exercise, you will write a function named `xor` that takes two
arguments, and returns `true` if exactly one of its arguments is truthy,
`false` otherwise. Note that we are looking for a boolean result instead of a
truthy/falsy value as returned by `||` and `&&`.

EXAMPLES:
xor?(5.even?, 4.even?) == true
xor?(5.odd?, 4.odd?) == true
xor?(5.odd?, 4.even?) == false
xor?(5.even?, 4.odd?) == false

BREAKDOWN:
- create a method that takes two arguments
  - are the arguments themselves always boolean return values?
  - or do they have to evaluate to truthy and falsey?
- method returns true if exactly one argument is truth and the other is falsey
- returns boolean

------------------------------------------------------------------------------

APPROACH (high level):
- create method `xor?(arg1, arg2)`
- if arg1 evaluates to truthy and arg2 evaluates to falsey
  - return true
- or if arg2 is truthy and arg1 is falsey
  - return true

- otherwise return false

TESTS:

PSEUDOCODE:
def xor?(param1, param2)
  if param1 is truthy and param2 is falsey
    return true
  else if  param2 is truthy and param1 is falsey
    return true
  else
    return false
  end
end
=end

def xor?(param1, param2)
  if param1 == true && param2 == false
    return true
  elsif param2 == true && param1 == false
    return true
  else
    return false
  end
end

p xor?(5.even?, 4.even?) == true
p xor?(5.odd?, 4.odd?) == true
p xor?(5.odd?, 4.even?) == false
p xor?(5.even?, 4.odd?) == false

