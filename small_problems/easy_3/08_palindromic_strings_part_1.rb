=begin
------------------------------------------------------------------------------
Write a method that returns true if the string passed as an
argument is a palindrome, false otherwise. A palindrome reads the
same forward and backward. For this exercise, case matters as does
punctuation and spaces.

EXAMPLES:
palindrome?('madam') == true
palindrome?('Madam') == false          # (case matters)
palindrome?("madam i'm adam") == false # (all characters matter)
palindrome?('356653') == true

BREAKDOWN:
- create a method that takes a string
- test if string is palindrome
  - case matters
  - spaces and punctuation matter
- return true if string is palindrome
- otherwise false.

------------------------------------------------------------------------------

APPROACH (high level):
- create a method `palindrome?(str)`
- test if `str` is palindrome
  - is `str` same backwards as forward
- return true  
- otherwise false

TESTS:

PSEUDOCODE:
def palindrome?(str)
  is `str` == `str` in reverse
  if true
  return true
  else
  false
end

REFACTOR:
Brute Force Method
- string is a collection
  - initialize new empty string
  - iterate through collection starting backwards
  - appending each element to another string collection
  - verify original string collection is the same as the new string collection
implementation:
- index = -1
- counter = 0
- start iteration on string[index] # => last item in string
- index -= 1
- counter += 1
- end iteration when counter == string.size
=end

# Brute Force With No Fancy Methods
def brute?(param)
  worker = ''
  counter = 0
  index = -1
  loop do
    worker << param[index]
    index -= 1
    counter += 1
    break if counter == param.size
  end
  param == worker
end
   
# Standard Solution, works with an array or string
def palindrome?(param)
  param == param.reverse
end

p palindrome?('madam') == true
p palindrome?('Madam') == false          # (case matters)
p palindrome?("madam i'm adam") == false # (all characters matter)
p palindrome?('356653') == true

p brute?('madam') == true
p brute?('Madam') == false          # (case matters)
p brute?("madam i'm adam") == false # (all characters matter)
p brute?('356653') == true

