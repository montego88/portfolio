=begin

# Problem
# Write a method that rotates an array by moving the first element to the end of the array. The original array should not be modified.
# Do not use the method Array#rotate or Array#rotate! for your implementation.
#
# input: array
# output: new array object
#
# explicit: original array is not modified, do not use Array#rotate or Array#rotate!
# implied: 
#
# Examples

rotate_array([7, 3, 5, 2, 9, 1]) == [3, 5, 2, 9, 1, 7]
rotate_array(['a', 'b', 'c']) == ['b', 'c', 'a']
rotate_array(['a']) == ['a']

x = [1, 2, 3, 4]
rotate_array(x) == [2, 3, 4, 1]   # => true
x == [1, 2, 3, 4]                 # => true

# Data structure: return array
#
# Algorithm:
# - (`rotate_array`) method with (`arr`) as parameter
#   - create empty array for results (`results`) 
#   - create temporary (`working_array`) 
#     - Implementation: Array#map
#   - Implementation: select first item of working array with Array#shift
#   - removes first item and returns new array with value (`first_item`) 
#   - iterate through remaining array items append to (`results`).
#
=end

def rotate_array(arr)
  working_array = arr.map { |item| item }
  results = []
  first_item = working_array.shift
  working_array.each {|item| results << item}
  results << first_item
end

p rotate_array([7, 3, 5, 2, 9, 1]) == [3, 5, 2, 9, 1, 7]
p rotate_array(['a', 'b', 'c']) == ['b', 'c', 'a']
p rotate_array(['a']) == ['a']
p 
p x = [1, 2, 3, 4]
p rotate_array(x) == [2, 3, 4, 1]   # => true
p x == [1, 2, 3, 4]                 # => true

