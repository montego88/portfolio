=begin
------------------------------------------------------------------------------
Write a method that takes two arguments, a positive integer and a boolean, and calculates the bonus for a given salary. If the boolean is `true`, the bonus should be half of the salary. If the boolean is `false`, the bonus should be `0`.

BREAKDOWN:
- method with 2 args (integer and boolean)
- calculate .5 of integer if boolean is true
- return integer 50% of given integer if true
- if boolean is false, method would always return 0?

EXAMPLES:
puts calculate_bonus(2800, true) == 1400
puts calculate_bonus(1000, false) == 0
puts calculate_bonus(50000, true) == 25000
------------------------------------------------------------------------------

APPROACH:
- divide given integer by `2` if boolean is true
- if boolean is false return 0

TEST CASES:

PSEUDOCODE:
def calculate_bonus(int, bool)
  if bool is true
    divide `int` by 2
  otherwise
    return 0
  end conditional statement
=end

def calculate_bonus(int, bool)
  return 0 unless bool == true
  int / 2
end

puts calculate_bonus(2800, true) == 1400
puts calculate_bonus(1000, false) == 0
puts calculate_bonus(50000, true) == 25000

