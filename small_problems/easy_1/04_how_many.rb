=begin
------------------------------------------------------------------------------
Write a method that counts the number of occurrences of each element in a given array.

EXAMPLES:
vehicles = [
  'car', 'car', 'truck', 'car', 'SUV', 'truck',
  'motorcycle', 'motorcycle', 'car', 'truck'
]

count_occurrences(vehicles)

The words in the array are case\-sensitive: `'suv' != 'SUV'`. Once counted, print each element alongside the number of occurrences.

Expected output:
car => 4
truck => 3
SUV => 1
motorcycle => 2

BREAKDOWN:
- count number of strings in an array
- each string is case-sensitive
- output string with number of occurence

- each string gets returned upon each iteration through array
- single or multi-dimensional arrays?
- create an additional solution for case-insensitive
------------------------------------------------------------------------------

APPROACH:
- create a method that takes an array (`count_occurrences(arr)`) 
- create working array (`working_array`) to use instead of mutating original array
- create (`counter`) set to 0 to access each element in array
- iterate through each index of array
  - create (`current_element`) variable assigned to current index
  - create a (`occurence`) variable to hold number of times same string occures in array
  - output string of `current_element => occurence`
  - end iteration when `counter` is the same size as the array

TEST CASES:
- case insensitive would be possible by using the #dowcase on each element of string.

PSEUDOCODE:

def count_occurrences(arr)
  - initialize a new local array to use that mirrors the same array given as an argument
  - iterate through new local array
  - save current element as local variable 
  - count how many times the first element occures and assign return value to local variable
  - remove element
  - output string made up of element and occurrence
  - end iteration
end
=end
vehicles = [
  'car', 'car', 'truck', 'car', 'SUV', 'truck',
  'motorcycle', 'motorcycle', 'car', 'truck', 'suv'
]

# case-insensitive solution
def count_occurrences(arr)
  working_array = arr.map{|element| element.downcase }
  loop do
    break if working_array == []
    current_element = working_array[0]
    occurrence = working_array.count(current_element)
    working_array.delete(current_element)
    # would be nice to map original case of `arr` when output
    puts "#{current_element.capitalize} => #{occurrence}"
  end
end

# case sensitive solution
#def count_occurrences(arr)
#  working_array = arr.map{|element| element}
#  loop do
#    break if working_array == []
#    current_element = working_array[0]
#    occurrence = working_array.count(current_element)
#    working_array.delete(current_element)
#    puts "#{current_element} => #{occurrence}"
#  end
#end
count_occurrences(vehicles)
