This solution shows a great way to use a hash when needing a key and value pair

tags: #hash, #array, #count

------------------------------------------------------------------------------

Solution

```ruby
def count_occurrences(array)
  occurrences = {}

  array.uniq.each do |element|
    occurrences[element] = array.count(element)
  end

  occurrences.each do |element, count|
    puts "#{element} => #{count}"
  end
end
```

#### Discussion

As we iterate over each unique element, we create a new key\-value pair in `occurrences`, with the key as the element's value. To count each occurrence, we use `Array#count` to count the number of elements with the same value.

Lastly, to print the desired output, we call `#each` on the newly created `occurrences`, which lets us pass the keys and values as block parameters. Then, inside of the block, we invoke `#puts` to print each key\-value pair.

#### Further Exploration

Try to solve the problem when words are case insensitive, e.g. `"suv" == "SUV"`.
