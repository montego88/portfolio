=begin
------------------------------------------------------------------------------
Write a method that takes one argument, an array containing integers, and
returns the average of all numbers in the array. The array will never be empty
and the numbers will always be positive integers. Your result should also be an
integer.

BREAKDOWN:
- method takes array of integers
- returns averaage of all numbers in array as single integer
- every array contains at least one integer
- all integers are positive

EXAMPLES:
puts average([1, 6]) == 3 # integer division: (1 + 6) / 2 -> 3
puts average([1, 5, 87, 45, 8, 8]) == 25
puts average([9, 47, 23, 95, 16, 52]) == 40

The tests above should print true.
------------------------------------------------------------------------------

APPROACH:
- define a method (`average`) that takes array (`arr`) 
- result =  get the sum of all integers and divide by `arr` length -- returns single integer
- return result

TEST CASES:

PSEUDOCODE:
def average(arr)
  initialize local variable `result` to hold return value of calculation
  calculation of average is sum of all integers in array divided by array length
  return `result`
end
=end

# RETURN FLOAT VERSION
#def average(arr)
#  result = arr.sum / arr.size.to_f
#  result.ceil(2)
#end

# RETURN INTEGER VERSION
def average(arr)
  result = arr.sum / arr.size
  result
end

puts average([1, 6]) == 3 # integer division: (1 + 6) / 2 -> 3
puts average([1, 5, 87, 45, 8, 8]) == 25
puts average([9, 47, 23, 95, 16, 52]) == 40

