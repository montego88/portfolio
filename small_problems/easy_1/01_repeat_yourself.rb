=begin
------------------------------------------------------------------------------
Write a method that takes two arguments, a string and a positive integer, and prints the string as many times as the integer indicates.

BREAKDOWN:
- define a method that takes a string and an integer
- output string as many times as integer 

EXAMPLES:
repeat('Hello', 3)

Hello
Hello
Hello

------------------------------------------------------------------------------

APPROACH:
Although there's a `#times` method that would be able to do this easily, It's best practice at this point to use a regular loop for practice.
- loop do with counter
- for loop 
- while loop

TEST CASES:

PSEUDOCODE:

BRUTE FORCE ITERATION IMPLEMENTATION:
- local variable (`counter`)
def repeat(str, int)
 loop do
   puts str 
   counter += 1
   break if counter == int
 end
end

USING A FOR LOOP:
def repeat(str, int)
  for i in 1..int do
  puts str
  end
end
  

USING A WHILE LOOP:
def repeat(str, int)
  counter = int
  while counter >= 0
    puts hello
  end
end

=end

# LOOP
def repeat(str, int)
  counter = 0
  loop do
    puts str 
    counter += 1
    break if counter == int
  end
end


# FOR LOOP
#def repeat(str, int)
#  for i in 1..int do
#  puts str
#  end
#end

 
# WHILE LOOP
#def repeat(str, int)
#  counter = int
#  while counter > 0
#    puts str
#    counter -= 1
#  end
#end

repeat('Hello', 3)
