This is a good resource for using some syntactical sugar and a helpful way to look at the `#remainder` method approach.


tags: #remainder, #sugar

Brute force:
```ruby
def digit_list(number)
  digits = []
  loop do
    number, remainder = number.divmod(10)
    digits.unshift(remainder)
    break if number == 0
  end
  digits
end
```
Idiomatic Ruby:
```ruby
def digit_list(number)
  number.to_s.chars.map(&:to_i)
end
```
#### Discussion

The brute force approach (which is not a derogatory term) chops off digits from the end of the number one at a time, adding them to an array, then returns the array. The tricky part of this loop is the use of `Integer#divmod` which divides `number` by `10`, and returns two values: the result of performing an integer division, and the remainder. For example, `12345.divmod(10)` returns `[1234, 5]`, and has the advantage of being easy to understand.

The second solution uses an idiomatic approach. Though somewhat dense, it's typical of the code you'll encounter (and write!) in Ruby all the time. Let's break it down.

Our goal is to convert a number to a list of its digits. First, we convert the number to a string, then split it into an array of numeric characters. This array is almost what we want, but we need an array of numbers, not an array of strings. `Enumerable#map` comes to the rescue. You might find the `(&:to_i)` part weird, but this is Ruby syntactic sugar; it's shorthand for:

```ruby
something.map { |char| char.to_i }
```


