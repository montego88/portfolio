=begin
------------------------------------------------------------------------------
Write a method that takes one argument, a positive integer, and returns a string of alternating 1s and 0s, always starting with 1. The length of the string should match the given integer.

BREAKDOWN:
- method takes a positive integer
- returns string of '1010..'
- return string should be same length as integer argument
- is there a minimum?

EXAMPLES:
puts stringy(6) == '101010'
puts stringy(9) == '101010101'
puts stringy(4) == '1010'
puts stringy(7) == '1010101'

------------------------------------------------------------------------------

APPROACH:
- define method to take one positive integer
- create a results string
- iterate `int` times
- start counter at 0
- append either `1` or `0` depending on iteration count if is even or odd 
- stop iteration when counter is same as `int`

TEST CASES:
irb: 0 is even?

PSEUDOCODE:
def stringy(int)
  initialize local variable `results` as empty string
  initialize local variable `counter` set to 1
  begin iteration
    if counter is odd
      append `1` to `results`
    othewise
      append `0` to `results`
    end conditional statement
  end iteration when `counter` is same as `int`
end
=end

def stringy(int, opt=1)
  results = ''
  counter = 1
  if (opt != 1) && (opt != 0)
    puts 'invalid second option'
    return
  end
  
  start_value = opt.to_s
  if opt == 1
    end_value = "0"
  else
    end_value = "1"
  end
  
  loop do
    if counter.odd?
      results << start_value
    else
      results << end_value
    end
    counter += 1
    break if counter > int
  end
  results
end
p stringy(6, 4) == nil #invalid argument
puts stringy(6, 1) == '101010'
puts stringy(6) == '101010'
puts stringy(6, 0) == '010101'
puts stringy(9) == '101010101'
puts stringy(4) == '1010'
puts stringy(7) == '1010101'

