=begin
------------------------------------------------------------------------------
Write a method that takes one argument, a string, and returns a new string with the words in reverse order.

BREAKDOWN:
- method takes string
- input: string
- output: new string of reversed word order from input string (not characters)
- if input is empty string, return empty string
- do all spaces equate to `''` or is that just for a string made up of empty spaces?

EXAMPLES:
puts reverse_sentence('Hello World') == 'World Hello'
puts reverse_sentence('Reverse these words') == 'words these Reverse'
puts reverse_sentence('') == ''
puts reverse_sentence('    ') == '' # Any number of spaces results in ''

------------------------------------------------------------------------------

APPROACH:
- define method `reverse_sentence(str)` to take a string argument
- create a results array
- assign new array to the conversion of a string into an array
- need to split each element of array by white space
- take last item from string array and append to results array
- finish when arrays are same size
- return array

TEST CASES:

PSEUDOCODE:
def reverse_sentence(str)
  - initialize new local array (`strings_array`)  from `str` conversion to array
  - initialize new local variable (`results`) array to empty
  - iteration
    - append `strings_array` last element to `results` array
    - end iteration when `strings_array` and `results` are same size.
  - return results as string instead of an array (include white space
end

=end

def reverse_sentence(str)
  strings_array = str.split
  results = []
  counter = (strings_array.size - 1)
  loop do 
    results << strings_array[counter]
    counter -= 1
    break if counter < 0
  end
  results.join(' ')
end

puts reverse_sentence('Hello World') == 'World Hello'
puts reverse_sentence('Reverse these words') == 'words these Reverse'
puts reverse_sentence('') == ''
puts reverse_sentence('    ') == '' # Any number of spaces results in ''


