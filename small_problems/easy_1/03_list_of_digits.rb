=begin
------------------------------------------------------------------------------
Write a method that takes one argument, a positive integer, and returns a list of the digits in the number.

BREAKDOWN:
- define a method that takes an integer object as an argument
- return list of digits in the number => array
- need to validate is an integer?
-  

EXAMPLES:
puts digit_list(12345) == [1, 2, 3, 4, 5]     # => true
puts digit_list(7) == [7]                     # => true
puts digit_list(375290) == [3, 7, 5, 2, 9, 0] # => true
puts digit_list(444) == [4, 4, 4]             # => true

------------------------------------------------------------------------------

APPROACH:
- define method and parameter `digit_list(int)`
  - create empty `results` array 
- iterating through collection of integers
  - `int` is a single number
  - because `int` is a single integer instead of a collection, we need a way to iterate through each integer
  - assign a new local variable to act as the string representation of the integer so that we can iterate through that collection (`str_rep`)
  - use loop with a block
    - increment counter
    - exit when counter is the same size as the `str_rep` collection
- append each index of string object to `results` array as integer
- return `results`

TEST CASES:
- iterate through an integer object?
- convert integer to string collection
- iterate through each string element and append as integer to array

PSEUDOCODE:
def digit_list(int)
  INITIALIZE LOCAL VARIABLE results TO EMPTY ARRAY
  INITIALIZE LOCAL VARIABLE str_rep TO THE CONVERSION OF int TO A STRING
  INITIALIZE LOCAL VARIABLE counter TO REPRESENT INDEX OF STRING COLLECTION
  ITERATE THROUGH STRING COLLECTION
    APPEND EACH STRING INDEX TO results array as integer
    INCREMENT counter
    EXIT ITERATION IF counter IS SAME VALUE AS Str_rep SIZE
  END
  RETURN results
end
=end

def digit_list(int)
  results = []
  str_rep = int.to_s
  counter = 0
  loop do
    results << str_rep[counter].to_i
    counter += 1
    break if counter == str_rep.size
  end
 results
end


puts digit_list(12345) == [1, 2, 3, 4, 5]     # => true
puts digit_list(7) == [7]                     # => true
puts digit_list(375290) == [3, 7, 5, 2, 9, 0] # => true
puts digit_list(444) == [4, 4, 4]             # => true
