=begin
------------------------------------------------------------------------------
Write a method that takes one integer argument, which may be positive, negative, or zero. This method returns true if the number's absolute value is odd. You may assume that the argument is a valid integer value.

BREAKDOWN:
- define a method with 1 integer as argument
- integer can be positive, negative or zero
- return true if integer absolute value is odd
- absolute value = how far a number is away from 0
  - remove `-` to evaluate

EXAMPLES:
puts is_odd?(2)    # => false
puts is_odd?(5)    # => true
puts is_odd?(-17)  # => true
puts is_odd?(-8)   # => false
puts is_odd?(0)    # => false
puts is_odd?(7)    # => true

------------------------------------------------------------------------------

APPROACH:
- define a method (`is_odd`) that takes an integer (`int`).
- do this with and without fancy integer
- without: take number and see if there is a remainder after dividing by 2. If remainder == 0 then number is even, else it is odd.
- with: take number apply `#odd?` to return boolean

=end

#def is_odd?(int)
#  int % 2 == 1
#end

def is_odd?(int)
  int.odd?
end

puts is_odd?(2)    # => false
puts is_odd?(5)    # => true
puts is_odd?(-17)  # => true
puts is_odd?(-8)   # => false
puts is_odd?(0)    # => false
puts is_odd?(7)    # => true
