=begin
------------------------------------------------------------------------------
Write a method that takes one argument, a positive integer, and returns the sum of its digits.

BREAKDOWN:
- method takes only a positive integer
- return sum of all digits

EXAMPLES:
puts sum(23) == 5
puts sum(496) == 19
puts sum(123_456_789) == 45
------------------------------------------------------------------------------

APPROACH:
- iterate through integer:
  - convert `int` to string
  - convert string to array
  - iterate through array of strings and append each string as an integer to `results` array
  - return sum of results elements

TEST CASES:

PSEUDOCODE:
def sum(int)
  initialize a local  empty array to hold results (`results`) 
  initialize local variable to hold string representation of `int` (`str_rep`) 
  initialize local array to hold return value from each element in the string collection (`string_array`) 
  iterate through `string_array`
    - append each element to `results` array as integer
  end iteration
  return sum of `results`
=end


# WITHOUT BASIC LOOPING CONSTRUCTS
def sum(int)
  results = []
  str_rep = int.to_s
  string_array = str_rep.split('')
  num = string_array.size
  num.times {|n| results << string_array[n].to_i }
  results.reduce(:+)
end

# WITH BASIC LOOPING CONSTRUCTS
#def sum(int)
#  results = []
#  str_rep = int.to_s
#  string_array = str_rep.split('')
#  string_array.each {|num| results << num.to_i }
#  results.reduce(:+)
#end

puts sum(23) == 5
puts sum(496) == 19
puts sum(123_456_789) == 45

