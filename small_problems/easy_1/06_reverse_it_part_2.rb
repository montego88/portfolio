=begin
------------------------------------------------------------------------------
Write a method that takes one argument, a string containing one or more words, and returns the given string with words that contain five or more characters reversed. Each string will consist of only letters and spaces. Spaces should be included only when more than one word is present.

BREAKDOWN:
- method takes one arg `str` containing one or more words
- returns new string? with characters reversed if word is greater than or equal to 5 characters
- each sting consists of only letters and spaces
- spaces should follow more than one word
- same string object? As in, does this mutate the original string? 

EXAMPLES:
puts reverse_words('Professional')          # => lanoisseforP
puts reverse_words('Walk around the block') # => Walk dnuora the kcolb
puts reverse_words('Launch School')         # => hcnuaL loohcS

------------------------------------------------------------------------------

APPROACH:
- define a method that takes a string
- create an array from string that includes each word as element
- count each separate element of string
- if count >= 5 reassign reversed word
- return array as string with white spaces


TEST CASES:
irb: confirm reversal of array element and reassignment

PSEUDOCODE:
def reverse_words(str)
  initialize new array (`results`)  from return value of string conversion
  iterate through new array object
  count each element
    if element length is equal to or greater than 5
      reverse element
    end conditional statment
  return string representation from `results` array 
end
=end


def reverse_words(str)
  results = str.split
  counter = 0
  loop do
    if results[counter].size >= 5
      results[counter].reverse!
    end
    counter += 1
    break if counter == results.size
  end
  results.join(' ')
end

puts reverse_words('Professional')          # => lanoisseforP
puts reverse_words('Walk around the block') # => Walk dnuora the kcolb
puts reverse_words('Launch School')         # => hcnuaL loohcS

