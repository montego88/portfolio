=begin
------------------------------------------------------------------------------
Write a method that takes two strings as arguments, determines the longest of
the two strings, and then returns the result of concatenating the shorter
string, the longer string, and the shorter string once again. You may assume
that the strings are of different lengths.

EXAMPLES:
short_long_short('abc', 'defgh') == "abcdefghabc"
short_long_short('abcde', 'fgh') == "fghabcdefgh"
short_long_short('', 'xyz') == "xyz"

BREAKDOWN:
- method takes two strings
- assigns longest string to (`longest`) 
- output not_longest longest not_longest
- assumption: strings are not same length

------------------------------------------------------------------------------

APPROACH (high level):
- create a method (`short_long_short(str1, str2)`) 
- compare length of each string
  - assign longest string to local variable (`long`) 

- output the concatenation of the not longest string with longest and again with not longest

TESTS:

PSEUDOCODE:
def short_long_short(str1, str2)
  if str1 length is greater than str2 length
    initialize local variable `long` = str1
    initialize local variable `short` = str2
  othewise
    initialize local variable `long` = str2
    initialize local variable `short = str1`
  end
  output short + long + short
end
=end

def short_long_short(str1, str2)
  if str1.size > str2.size
    long = str1
    short = str2
  else
    long = str2
    short = str1
  end
  short + long + short
end

p short_long_short('abc', 'defgh') == "abcdefghabc"
p short_long_short('abcde', 'fgh') == "fghabcdefgh"
p short_long_short('', 'xyz') == "xyz"

