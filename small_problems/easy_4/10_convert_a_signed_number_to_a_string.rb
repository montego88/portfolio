=begin

PROBLEM:
in the previous exercise, you developed a method that converts non\-negative numbers to strings. In this exercise, you're going to extend that method by adding the ability to represent negative numbers as well.

Write a method that takes an integer, and converts it to a string representation.

You may not use any of the standard conversion methods available in Ruby, such as `Integer#to_s`, `String()`, `Kernel#format`, etc. You may, however, use `integer_to_string` from the previous exercise.

Examples:
signed_integer_to_string(4321) == '+4321'
signed_integer_to_string(-123) == '-123'
signed_integer_to_string(0) == '0'

--------------------------------------------------------------

BREAKDOWN: take an signed integer and convert to a string representation of the integers 


DATA STRUCTURES:
input: integers
output: string representation of the integers


APPROACH: 
- take the integer and convert to a string 
  - convert from integer to an array of with each single number as consecutive index 
  - including the sign of + or - accodingly
- return string

		
PSEUDOCODE:
def signed_integer_to_string(int)
  initialize a local variable for return value `results`
  initialize a hash to hold the key, value pairs with keys as strings and values as integers
  initialize an empty array to work with `arr`
  append the results of calculations for each number to `arr`  
  append the string `+` if the number is not negative
    - multiply `int` by -1 and if result is negative then number is positive
    - `-` otherwise
    - unless zero, do not append `+`


IMPLEMENTAION/NOTES:
Integer.digits performs the integer to an array but I would like to come up with a more manual method

in order to do this via calculations and for my own math practice, I need to count how many digits are in the number. the one way I know about is to use the provided Integer#digits method to create an array of the digits and then get the size
- 

=end
def signed_integer_to_string(int)
  results = ''
  sign = '+'
  if int == 0
    sign = ''
  elsif is_negative?(int)
    sign = '-'
    int = (int * -1)
  end
  arr = int.digits.reverse
  results << sign + arr.join
end

def is_negative?(int)
  int.negative?
end

p signed_integer_to_string(4321) == '+4321'
p signed_integer_to_string(-123) == '-123'
p signed_integer_to_string(0) == '0'
