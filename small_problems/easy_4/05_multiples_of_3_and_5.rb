=begin
------------------------------------------------------------------------------
Write a method that searches for all multiples of 3 or 5 that lie between 1 and
some other number, and then computes the sum of those multiples. For instance,
if the supplied number is `20`, the result should be `98` (3 + 5 + 6 + 9 + 10 +
12 + 15 + 18 + 20).

You may assume that the number passed in is an integer greater than 1.

BREAKDOWN:
find multiples of 3 OR 5
- starting with 1 inclusive to number passed
- compute sum of found multiples


EXAMPLES:
multisum(3) == 3
multisum(5) == 8
multisum(10) == 33
multisum(1000) == 234168

------------------------------------------------------------------------------

APPROACH (high level):
- method takes one integer (`int`) as argument
- range of 1..`int`
  - iterate through range
    - current number multiple of 3?
      - append to array (`multiples_of_three`) 
- range of 1..`int`
  - iterate through range
    - current number multiple of 5?
      - append to array (`multiples_of_five`) 
sum of both sums


TEST/IMPLEMENTAION NOTES:

PSEUDOCODE:
def mutisum(int)
  initialize local variable `multiples_of_three` assigned to an empty array
  initialize local variable `multiples_of_five` assigned to an empty array
  iterate through numbers 1..`int` and find multiples of 3 or 5
    - if number divides evenly it is a multiple of num
      - append to appropriate array
  add up sums of individual arrays
  and sum of sums
=end

def multisum(int)
  multiples_of_three = []
  multiples_of_five = []
  (1..int).each do |num|
    if num % 3 == 0
      multiples_of_three << num
    elsif num % 5 == 0
      multiples_of_five << num
    end
  end
    sum = multiples_of_three.sum
    sum += multiples_of_five.sum
    sum
end

p multisum(3) == 3
p multisum(5) == 8
p multisum(10) == 33
p multisum(1000) == 234168
