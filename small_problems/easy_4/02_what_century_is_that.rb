=begin
------------------------------------------------------------------------------
Write a method that takes a year as input and returns the century. The return
value should be a string that begins with the century number, and ends with
`st`, `nd`, `rd`, or `th` as appropriate for that number.

New centuries begin in years that end with `01`. So, the years 1901\-2000
comprise the [20th century](https://en.wikipedia.org/wiki/20th_century).

BREAKDOWN:
- define a method that takes an integer
- return value is string representation of century and a suffix
- suffix needs to be appropriate to the number of the century

EXAMPLES:
century(2000) == '20th'
century(2001) == '21st'
century(1965) == '20th'
century(256) == '3rd'
century(5) == '1st'
century(10103) == '102nd'
century(1052) == '11th'
century(1127) == '12th'
century(11201) == '113th'

------------------------------------------------------------------------------

APPROACH:
- method to return string representation
  - identify a pattern for the century equation
    - look like most are year / 100 + 1
    - year 2000 is exempt from this because it is evenly divided by 100 whereas the others are not
  - create an appropriate suffix
    - identify a pattern for the century suffix
      - 'st' => 1
      - 'nd' => 2
      - 'rd' => 3
      - 'th' => everything else
        - except 11 should with 'th'
        - except 12 should with 'th'
        - except 13 should with 'th'

TEST/IMPLEMENTATION NOTES:

PSEUDOCODE:
def suffix(str)
  # example str = '21'
  Evaluate if str ends with 11, 12 or 13
    - if so, suffix is 'th'
  othewise evaluate `str`
    - if last element is 1 suffix is 'st'
    - if last element is 2 suffix is 'nd'
    - if last element is 3 suffix is 'rd'
    otherwise suffix is 'th'
    return suffix as string
end

def century(year)
  - initialize a local variable (`century_value`) assigned to the return value of `year` / 100 + 1
    - if year is evenly divided by 100 then `century_value` is `year` / 100
  - initialize a local variable (`result`) for the string representation of `century_value`
  - pass `result` to suffix helper method
end
=end
def suffix(str)
  if ['11', '12', '13'].include?(str[-2..-1])
    return 'th'
  end

  case str[-1]
  when '1'
    'st'
  when '2'
    'nd'
  when '3'
    'rd'
  else
    'th'
  end
end

def century(year)
  century_value = year / 100 + 1
  if year % 100 == 0
    century_value -= 1
  end
  result = century_value.to_s
  result << suffix(result)
end

p century(2000) == '20th'
p century(2001) == '21st'
p century(1965) == '20th'
p century(256) == '3rd'
p century(5) == '1st'
p century(10103) == '102nd'
p century(1052) == '11th'
p century(1127) == '12th'
p century(11201) == '113th'

