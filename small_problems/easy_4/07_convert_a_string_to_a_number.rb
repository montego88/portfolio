=begin
------------------------------------------------------------------------------
The `String#to_i` method converts a string of numeric characters (including an
optional plus or minus sign) to an `Integer`. `String#to_i` and the `Integer`
constructor (`Integer()`) behave similarly. In this exercise, you will create a
method that does the same thing.

Write a method that takes a String of digits, and returns the appropriate
number as an integer. You may not use any of the methods mentioned above.

For now, do not worry about leading `+` or `-` signs, nor should you worry
about invalid characters; assume all characters will be numeric.

You may not use any of the standard conversion methods available in Ruby to
convert a string to a number, such as `String#to_i`, `Integer()`, etc. Your
method should do this the old\-fashioned way and calculate the result by
analyzing the characters in the string.

BREAKDOWN: take a string of digits and return the numerical representation of given string.
- method take a string of digits
- converts string to integers
  - cannot use standard methods for conversion
- return number

EXAMPLES:
string_to_integer('4321') == 4321
string_to_integer('570') == 570

------------------------------------------------------------------------------

APPROACH: Create a method that takes a string as its input and retunrs the numerical representation from the input. 
- create a `results` array to contain the integers
- create a method that takes a string as a parameter
- create an hash that has the digits from 0-9 representing the value and the keys are the string representation of the values i.e. '0'=>0
- create an array from splitting each element from each other to their own indices.
- iterate through the created array 
  - compare to the hash to find the key 
    - append value to `results` array
- convert array of numbers `results` to an integer that represents the numbers combined into one (math)
- return the number

- define a method (`string_to_integer(str)`) 
- assign an empty array to `results`
- iterate through each element in string collection
- see about making an array of stringed integers and matching the string value with the associated index
  - or better a hash? {0: '0'} `digits`

TEST CASES:

PSEUDOCODE:
def string_to_integer(str)
  initialize local variable `results`
  initialize a local hash `integers` to hold the string and numeric values 0-9
  iterate through `integers`
    append the numeric value to the `results` array
  exit iteration
  pass results array to helper method `conversion`
  return conversion
end

def conversion(arr)
  length = arr.size - 1 
  counter = 1
  number = 0
  iterate `length` times and multiply counter * 10 each time
  end iteration
  iterate through each individual element of `arr`
    - append current element * counter to number
    - decrement by dividing counter by 10
  end iteration
  return number    
end
=end
def string_to_integer(str)
  results = []
  integers = {'0'=>0, '1'=>1, '2'=>2, '3'=>3, '4'=>4, '5'=>5, '6'=>6, '7'=>7, '8'=>8, '9'=>9}
  counter = 0
  loop do
    integers.each do |key, value|
      results << value if key == str[counter]
    end
    counter += 1
    break if counter == str.size
  end
  results
  conversion(results)
end

def conversion(arr)
  length = arr.size - 1
  counter = 1
  number = 0
  length.times do |i|
    counter *=  10
  end
  arr.each do |value|
    number += value * counter
    counter /= 10
  end
  number
end

p string_to_integer('4321') == 4321
p string_to_integer('570') == 570
