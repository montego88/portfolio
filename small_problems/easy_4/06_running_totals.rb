=begin
------------------------------------------------------------------------------
Write a method that takes an Array of numbers, and returns an Array with the
same number of elements, and each element has the running total from the
original Array.

BREAKDOWN:
- define method that takes an array of integers
- return a new array the same length of original array
  - new array has running total from original array


EXAMPLES:
running_total([2, 5, 13]) == [2, 7, 20]
running_total([14, 11, 7, 15, 20]) == [14, 25, 32, 47, 67]
running_total([3]) == [3]
running_total([]) == []

------------------------------------------------------------------------------

APPROACH/ALGORITHM (high level):
- define new method (`running_total(arr)`) 
- assign new array (`results`) 
- assign results[0] = array[0]
- assign each following `results` index to sum of `arr` index + 1
- return array


TEST/IMPLEMENTAION NOTES:
- 

PSEUDOCODE:
def running_total(arr)
  - initialize empty `results` array 
  - `results[0] is the same starting value as `arr[0]`
  - iterate through arr
    - if index is > 0
      append sum of index and (index - 1) to `results`
  - exit iteration when `results` is same length as `arr`
  - return `results`
end
=end

def running_total(arr)
  results = []
  counter = 0
  loop do
    break if results.size == arr.size
    if counter == 0
      results << arr[counter]
    else
      results << (arr[counter] + results[counter - 1])
    end
    counter += 1
  end
  results
end

p running_total([2, 5, 13]) == [2, 7, 20]
p running_total([14, 11, 7, 15, 20]) == [14, 25, 32, 47, 67]
p running_total([3]) == [3]
p running_total([]) == []

