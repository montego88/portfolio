=begin

PROBLEM:
Write a method that takes a positive integer or zero, and converts it to a string representation.

You may not use any of the standard conversion methods available in Ruby, such as `Integer#to_s`, `String()`, `Kernel#format`, etc. Your method should do this the old\-fashioned way and construct the string by analyzing and manipulating the number.

Examples:
integer_to_string(4321) == '4321'
integer_to_string(0) == '0'
integer_to_string(5000) == '5000'

--------------------------------------------------------------

BREAKDOWN: take a integer as an argument and return the string representation of that argument.
- convert the integer to an array
- work on array and match integer with string representation
- return string



DATA STRUCTURES:
Input: intege
Output: string


APPROACH: 
- create a hash to represent the key, value pairs of each string and numeric representation respectively
- convert integer to array
- match 'container' elements to their associated hash key appending each to a new array as hash keys (strings)
- return joined new array
		
PSEUDOCODE:

IMPLEMENTAION/NOTES:

=end
def integer_to_string(int)
  result = []
  hash = {"0"=>0, "1"=>1, "2"=>2, "3"=>3, "4"=>4, "5"=>5, "6"=>6, "7"=>7, "8"=>8, "9"=>9}
  numbers = int.digits.reverse
  numbers.join
end

p integer_to_string(90) == '90'
p integer_to_string(4321) == '4321'
p integer_to_string(0) == '0'
p integer_to_string(5000) == '5000'
