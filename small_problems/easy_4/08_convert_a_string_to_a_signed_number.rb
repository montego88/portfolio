=begin

PROBLEM:
In the previous exercise, you developed a method that converts simple numeric strings to Integers. In this exercise, you're going to extend that method to work with signed numbers.

Write a method that takes a String of digits, and returns the appropriate number as an integer. The String may have a leading `+` or `-` sign; if the first character is a `+`, your method should return a positive number; if it is a `-`, your method should return a negative number. If no sign is given, you should return a positive number.

You may assume the string will always contain a valid number.

You may not use any of the standard conversion methods available in Ruby, such as `String#to_i`, `Integer()`, etc. You may, however, use the `string_to_integer` method from the previous lesson.

Examples:

string_to_signed_integer('4321') == 4321
string_to_signed_integer('-570') == -570
string_to_signed_integer('+100') == 100

--------------------------------------------------------------

BREAKDOWN: Take a string representation of a number and return the number as an integer
- string may or may not have a leading `+`. 
- Otherwise it may have a leading `-` sign
- return a positive number unless negative is declared
  - return negative number


DATA STRUCTURES:
Input: string
Output: Integer


APPROACH: take a string and determine if it should be positive or negative. return numerical representation of string.  
- create an array made of string elements from the string `characters`
- create container array `digits`
- create `sign` = ''
- create a hash to hold key, value pairs `integers`
  - where keys are strings and values are integers
- iterate through `characters` array
  - return matching integer value to `digits' to hold the integers.
- determine if number is positive or negative and reassign `sign` to 'negative' if negative
  - get first character of `characters`
      if character is `-`
        - reassign `sign` to 'negative'
      else
        - reassign `sign` to 'positive'
      end
- pass `digits` to a helper method
  - take each digit and perform math
    - create a `signed_number` to return 
    - create a counter set to `1`
    - create a size variable `length` to hold the size of the array - 1
    - iterate `length` times and mulitply counter by 10 reassigning counter accordingly
    - iterate through given array
      - append the current value * counter to the `signed_number`
      - decrement counter divided and reassigned by 10
- return signed number
		
PSEUDOCODE:

IMPLEMENTAION/NOTES:

=end
def conversion(arr)
  signed_number = 0
  counter = 1
  length = arr.size - 1
  length.times do 
    counter *= 10
  end

  arr.each do |ele|
    signed_number += ele * counter
    counter /= 10
  end

  signed_number
end

def string_to_signed_integer(str)
  characters = str.chars
  digits = []
  integers = {'0'=>0, '1'=>1, '2'=>2, '3'=>3, '4'=>4, '5'=>5, '6'=>6, '7'=>7, '8'=>8, '9'=>9}
  counter = 0
  loop do
    if integers.key?(characters[counter])
      digits << integers[characters[counter]]
    else
      counter += 1
      next
    end
    counter += 1
    break if counter == characters.size
  end
  if characters[0] == '-'
    conversion(digits) * -1
  else
    conversion(digits)
  end
end

p string_to_signed_integer('4321') == 4321
p string_to_signed_integer('-570') == -570
p string_to_signed_integer('+100') == 100
