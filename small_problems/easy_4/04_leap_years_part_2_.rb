=begin
------------------------------------------------------------------------------
A continuation of the previous exercise.

The British Empire adopted the Gregorian Calendar in 1752, which was a leap
year. Prior to 1752, the Julian Calendar was used. Under the Julian Calendar,
leap years occur in any year that is evenly divisible by 4.

Using this information, update the method from the previous exercise to determine leap years both before and after 1752.

EXAMPLES:
leap_year?(2016) == true
leap_year?(2015) == false
leap_year?(2100) == false
leap_year?(2400) == true
leap_year?(240000) == true
leap_year?(240001) == false
leap_year?(2000) == true
leap_year?(1900) == false
leap_year?(1752) == true
leap_year?(1700) == true
leap_year?(1) == false
leap_year?(100) == true
leap_year?(400) == true

BREAKDOWN:
- evaluate if year is gregorian or julian
- pass year into one of two different methods (`gregorian_leap_year?(year)`) or (`julius_leap_year?(year)`) 
- return boolean

------------------------------------------------------------------------------

APPROACH (high level):
- create one method that access two helper method return values
  - julius_leap_year(year)
  - gregorian_leap_year(year)
- evaluate if year is 1752 or earlier
- if true, pass year argument to `julius_leap_year(year)`
- else
- pass year to `gregorian_leap_year(year)`

TESTS:

PSEUDOCODE:
def gregorian_leap_year?(year)
  if year % 4 != 0
    false
  elsif year % 100 != 0
    true
  elsif year % 400 == 0
    true
  else
    false
  end
end

def julius_leap_year?(year)
  if year % 4 == 0
    true
  else
    false
  end
end

def leap_year?(year)
  if year is <= 1752
    julius_leap_year?(year)
  else
    gregorian_leap_year?(year)
  end
=end


def gregorian_leap_year?(year)
  if year % 4 != 0
    false
  elsif year % 100 != 0
    true
  elsif year % 400 == 0
    true
  else
    false
  end
end

def julius_leap_year?(year)
  if year % 4 == 0
    true
  else
    false
  end
end

def leap_year?(year)
  if year <= 1752
    julius_leap_year?(year)
  else
    gregorian_leap_year?(year)
  end
end

p leap_year?(2016) == true
p leap_year?(2015) == false
p leap_year?(2100) == false
p leap_year?(2400) == true
p leap_year?(240000) == true
p leap_year?(240001) == false
p leap_year?(2000) == true
p leap_year?(1900) == false
p leap_year?(1752) == true
p leap_year?(1700) == true
p leap_year?(1) == false
p leap_year?(100) == true
p leap_year?(400) == true
