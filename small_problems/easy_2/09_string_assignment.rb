=begin
------------------------------------------------------------------------------
Take a look at the following code:

name = 'Bob'
save_name = name
name = 'Alice'
puts name, save_name

What does this code print out? Think about it for a moment before continuing.

If you said that code printed:

Alice
Bob

you are 100% correct, and the answer should come as no surprise. Now, let's look at something a bit different:

name = 'Bob'
save_name = name
name.upcase!
puts name, save_name

What does this print out? Can you explain these results?

BREAKDOWN:

In the second example, we first initialize a new local variable with the name of `name` to the string object `Bob`. We then initialize a new local variable with the name of `save_name` and assign it to the exact same string object as `name`. We now have two different local variables pointing to the same string object with a value of `Bob` in this case. When we next invoke the `#upcase!` method by calling it on the local variable `name` which mutates the caller because `#upcase!` is a mutating method invocation. This means that the `name.upcase!` has not reassigned `name` to a new string object, it has instead permanently modified the original string object. Therefore both `name` and `save_name` now point to the same, albeit mutated, string object with a value of `BOB`. 

=end
