=begin
------------------------------------------------------------------------------
Build a program that displays when the user will retire and how many years she
has to work till retirement.

BREAKDOWN:
- ask What is your age?
- ask At what age would you like to retire?
- output return


EXAMPLES:
What is your age? 30
At what age would you like to retire? 70

It's 2016. You will retire in 2056.
You have only 40 years of work to go!

------------------------------------------------------------------------------

APPROACH:
- prompt user for input
- save reply
- prompt user for input
- save reply
- calculate target age - age
- get current year
- calculate current year with target age - age return value

TEST CASES:
t=Time.new
current_year = t.year
PSEUDOCODE:
def what_year_retire
  prompt user for input
  initialize local variable to user response (`age`) 
  promot user for input
  initialize local variable for user response (`target_age`) 
  initialize local variable for calculation of `target_age - age` (`years_to_go`)
  initialize local variable for current year (`current_year`) 
  initialize local variable for `current_year + years_to_go` (`total_years`) 
=end

def what_year_retire
  puts "What is your age?"
  age = gets.chomp.to_i
  puts "At what age would you like to retire?"
  target_age = gets.chomp.to_i
  years_to_go = (target_age - age)
  t = Time.new
  current_year = t.year
  total_years = (current_year + years_to_go)
  puts "It's #{current_year}. You will retire in #{total_years}."
  puts "You have only #{years_to_go} years of work to go!"
end

what_year_retire

