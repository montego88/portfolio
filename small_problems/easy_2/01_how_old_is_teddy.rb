=begin
------------------------------------------------------------------------------
Build a program that randomly generates and prints Teddy's age. To get the age,
you should generate a random number between 20 and 200.

BREAKDOWN:
- method takes two integers representing a min and max value
- generate random number between the `min` and `max` value
- method outputs string

EXAMPLES:

Example Output
Teddy is 69 years old!
------------------------------------------------------------------------------

APPROACH:
- define method with two integer parameters
- return a random number between both given integers
- output string

TEST CASES:

PSEUDOCODE:
def how_old_is_teddy(min, max)
  initialize local variable for random value: (`random`) 
    - generate random value from between `min` and `max` values assign return value to `random`
  output string and string interpolation of `random`

def how_old_is_name(min, max, name='Teddy')
=end

def how_old_is_name(min, max, name='Teddy')
  random = rand(min..max)
  puts "#{name} is #{random} years old!"
end

def how_old_is_teddy(min, max)
  random = rand(min..max)
  puts "Teddy is #{random} years old!"
end

how_old_is_teddy(20, 200)
how_old_is_name(20, 100) 
