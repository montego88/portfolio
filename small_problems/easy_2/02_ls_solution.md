This is a good overview of using floats and returning a rounded float where necessary. I can think of a few different implementations to this going from metric to imperial of course.

tags: #round, #decimal, #floats, #ceil

Solution


```ruby
SQMETERS_TO_SQFEET = 10.7639

puts '==> Enter the length of the room in meters: '
length = gets.to_f

puts '==> Enter the width of the room in meters: '
width = gets.to_f

square_meters = (length * width).round(2)
square_feet = (square_meters * SQMETERS_TO_SQFEET).round(2)

puts "The area of the room is #{square_meters} " + \
     "square meters (#{square_feet} square feet)."
```

#### Discussion

Our solution is straightforward. First we obtain the length, then we get the width. Next we perform our calculations, and then we print the results. We do no validation on our inputs, and just assume the user will enter appropriate values.

We use a constant, `SQMETERS_TO_SQFEET` to store the conversion factor between square meters and square feet. This is good practice any time you have a number whose meaning is not immediately obvious upon seeing it.

The only thing that may be unfamiliar here is the `round` method (a method of the `Float` class), which is used to round our results to the nearest 2 decimal places. (You can also use `Kernel#format` to format the results, but `format` is harder to use.)

#### Further Exploration

Modify this program to ask for the input measurements in feet, and display the results in square feet, square inches, and square centimeters.
