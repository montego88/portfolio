=begin
------------------------------------------------------------------------------
Write a program that will ask for user's name. The program will then greet the user. If the user writes "name!" then the computer yells back to the user.

BREAKDOWN:
- ask user's name
- output greeting to user
- if computer end the input with an !
- output greeting and message in all caps

EXAMPLES:
What is your name? Bob
Hello Bob.

What is your name? Bob!
HELLO BOB. WHY ARE WE SCREAMING?

------------------------------------------------------------------------------

HIGH LEVEL APPROACH:
- ask for user input
- save input as variable `name`
  - inspect `name`
  - depending on string input, construct output
- return output string using variable `name`

PSEUDOCODE:
def greeting
  prompt user for input `name`
  initialize local variable for returned string (`name`) 
  inspect `name` to see if the last character in string == !
    - if string collection last element == '1'
    - construct output string in all caps and append extra message
  otherwise
    - construct simple greeting
end

TESTS:
string[string.size - 1]
=end

def greeting
  puts "What is your name?"
  name = gets.chomp
  if name[name.size - 1] == '!'
    output = "Hello #{name.chop}. Why are we screaming?" 
    puts output.upcase
  else
    puts "Hello #{name}."
  end
end

greeting
