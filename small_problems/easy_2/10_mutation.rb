=begin
What will the following code print, and why? Don't run the code until you have tried to answer.

array1 = %w(Moe Larry Curly Shemp Harpo Chico Groucho Zeppo)
array2 = []
array1.each { |value| array2 << value }
array1.each { |value| value.upcase! if value.start_with?('C', 'S') }
puts array2


- On line 1 we initialize a new local variable `array1` that is assigned to the return value of the new array, in this case: `["Moe", "Larry", "Curly", "Shemp", "Harpo", "Chico", "Groucho", "Zeppo"]`
- On line 2 we then assign the local variable `array2` to an empty array object
- We next call the method `#each` on the `array1` local variable to iterate through each element in the array and append the element to the local variable `array2` that is scoped outside of the block and is accessible within the block. This shows us how outer scoped variables are still accessible when working with method invocations that take a block as an argument.
- On the next line, we call the method `#each` again on the local variable but this time while iterating through the array's elements, we call the destructive method `#upcase!` on each element of calling object that matches the criteria of an element starting with either a `C` or an `S`, in this case `Curly`,`Shemp`, and `Chico`. This results in mutation of the specific objects in the array as they are changed permanently by calling the mutation method `#upcase!` on them. The are not new string object, but simply a permanently changed string object.

- Finally, we output the value of `array2` which will output the same, although mutated strings as `array1`. In fact, `array1` and `array2` are still referencing the exact same array object, it has been mutated though because it's elements have changed.

=end




