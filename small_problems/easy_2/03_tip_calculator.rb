=begin
------------------------------------------------------------------------------
Create a simple tip calculator. The program should prompt for a bill amount and a tip rate. The program must compute the tip and then display both the tip and the total amount of the bill.

BREAKDOWN:
- ask user for bill price
- ask user for the percentage of the tip
- compute the tip (bill * .15) 
- output strings

EXAMPLES:
What is the bill? 200
What is the tip percentage? 15

The tip is $30.0
The total is $230.0

------------------------------------------------------------------------------

APPROACH:
- create a method (`tip_calculator`)
- ask user two questions for input
  - bill
  - percentage
- assign input to (`bill`) 
- assign input to (`percentage`) 
- tip is the return value from bill * percentage as decimal (e.g 15% == .15)
- output total tip
- output combined total

TEST CASES:

PSEUDOCODE:
def tip_calculator
  ask for user input
  intialize local variable `bill` and assign return value from user input 
    - `bill` needs to be converted to float 
  initialize local variable `percentage` and assign the return value from the user input
    - percentage needs to be converted from string to decimal (percentage / 100)
  initialize local variablem `tip` and assign the return value from bill * percentage
  initialize local variable `grand_total`
  output total tip in string format
  output grand_total string
=end

def tip_calculator
  puts "What is the bill?"
  bill = gets.chomp.to_f
  puts "What is the tip percentage?"
  percentage = gets.chomp.to_f
  decimal = percentage / 100
  tip = (bill * decimal)
  grand_total = bill + tip
  puts "The tip is $#{tip}"
  puts "The total is $#{grand_total}"
end

tip_calculator

