=begin
------------------------------------------------------------------------------
Build a program that asks a user for the length and width of a room in meters and then displays the area of the room in both square meters and square feet.

Note: 1 square meter == 10.7639 square feet

Do not worry about validating the input at this time.

BREAKDOWN:
- ask user input
- width
- length
- display area of room in square meters and square feet

EXAMPLES:
Enter the length of the room in meters:
10
Enter the width of the room in meters:
7
The area of the room is 70.0 square meters (753.47 square feet).

------------------------------------------------------------------------------

APPROACH:
- get user input `length` in meters
- get user input `width` in meters
- idea: nice to have user input which measurement scale
- calculate square meters area (length * width)
- convert square meters to square feet 1 square meter == 10.7639 square feet

TEST CASES:

PSEUDOCODE:
define get_square_area
  - ask user for input `length`
  - assign user input to local variable (`length`) 
  - ask user for input `width`
  - assign user input to local variable (`width`) 
  - assign local variable to return value of the product from `length` * `width` (`square_meters`) 
  - assign local variable to calculation of `square_meters` * 10.7639 (`square_feet`) 
  - output the string representation of `square_meters` and `square_feet`
=end

def get_square_area
  puts "Enter the length of the room in meters:"
  length = gets.chomp
  puts "Enter the width of the room in meters:"
  width = gets.chomp
  square_meters = (length.to_f * width.to_f)
  square_feet = (square_meters * 10.7639)
  puts "The area of the room is #{square_meters} square meters (#{square_feet.round(2)} square feet)."
end
get_square_area 
