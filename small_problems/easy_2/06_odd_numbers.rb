=begin
------------------------------------------------------------------------------
Print all odd numbers from `1` to `99`, inclusive, to the console, with each number on a separate line.

BREAKDOWN:
- print odd numbers from a range
- each number on separate line

EXAMPLES:

------------------------------------------------------------------------------

APPROACH:
- create a method that takes a range
- output each odd integer on new line

TEST CASES:
irb: range syntax
      create an array from a range of integers
PSEUDOCODE:
def odds(x, y)
  create a range starting with `x` up to and including `y`
  iterate through each number
  output the number with line break if the number is odd
end
=end

#UPTO
#def odds(x, y)
#  x.upto(y) {|num| puts num if num.odd?}
#end

#SELECT
#def odds(x, y)
#  tmp_arr = (x..y).to_a
#  results = tmp_arr.select {|num| num.odd?}
#  results.each {|num| puts num}
#end

# MY FIRST SOLUTION
def odds(x, y)
(x..y).each do |num|
  puts num if num.odd?
  end
end

odds(1, 99)
