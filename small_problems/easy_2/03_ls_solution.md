This was a simple problem but again I noticed I stumbled upon the use of decimals (floats) and the strange amount of integers. This makes sense of course

tags: #decimal, #rounding, #round

Solution


```ruby
print 'What is the bill? '
bill = gets.chomp
bill = bill.to_f

print 'What is the tip percentage? '
percentage = gets.chomp
percentage = percentage.to_f

tip   = (bill * (percentage / 100)).round(2)
total = (bill + tip).round(2)

puts "The tip is $#{tip}"
puts "The total is $#{total}"
```

Discussion

We first obtain the amount of the bill from the user, not worrying too much about data validity for this simple problem. Then we get the tip percentage.

Next, we calculate the actual tip, and the total bill, then output the results.
Further Exploration

Our solution prints the results as $30.0 and $230.0 instead of the more usual $30.00 and $230.00. Modify your solution so it always prints the results with 2 decimal places.

Hint: You will likely need `Kernel#format` for this.

See previous location [tip calculator](file:///Users/monte/Projects/launch-school/rb109_assessment_ruby_and_general_programming/notes/part_1_written_assessment_study_guide/study-exercises/small_problems/easy_2/03_tip_calculator.rb)
Also see the [Kernel#format docs](https://ruby-doc.org/core-3.0.0/Kernel.html#method-i-format)
