This was a learning curve on my own part simply because I accidetnally skipped a step, but in the end I froze trying to figure out the solution to the situation I had created. I panicked with the time frame and jumped into hack-n-slash mode which worked but I lost time with it and couldn't articulate it correctly.

Spend some time learning more about [Inject Module: Enumerable (Ruby 3.0.0)](https://ruby-doc.org/core-3.0.0/Enumerable.html#method-i-inject)

tags: #inject, #reduce, #enumerable

Solution


```ruby
def compute_sum(number)
  total = 0
  1.upto(number) { |value| total += value }
  total
end

def compute_product(number)
  total = 1
  1.upto(number) { |value| total *= value }
  total
end

puts ">> Please enter an integer greater than 0"
number = gets.chomp.to_i

puts ">> Enter 's' to compute the sum, 'p' to compute the product."
operation = gets.chomp

if operation == 's'
  sum = compute_sum(number)
  puts "The sum of the integers between 1 and #{number} is #{sum}."
elsif operation == 'p'
  product = compute_product(number)
  puts "The product of the integers between 1 and #{number} is #{product}."
else
  puts "Oops. Unknown operation."
end
```

#### Discussion

For brevity and simplicity, our solution doesn't try too hard to validate the user input. Your own solution probably should try to validate input and issue error messages as needed.

This solution first obtains the integer and operation to be performed from the user, then we perform the requested operation using one of two methods: `compute_sum` adds the numbers together, while `compute_product` multiplies them. Once we have the result, we just print it.

#### Further Exploration

The `compute_sum` and `compute_product` methods are simple and should be familiar. A more rubyish way of computing sums and products is with the `Enumerable#inject` method. `#inject` is a very useful method, but if you've never used it before, it can be difficult to understand.

Take some time to read the documentation for `#inject`. (Note that all `Enumerable` methods can be used on Array.) Try to explain how it works to yourself.


Try to use `#inject` in your solution to this problem.
