=begin
------------------------------------------------------------------------------
Write a program that asks the user to enter an integer greater than 0, then
asks if the user wants to determine the sum or product of all numbers between 1
and the entered integer.

BREAKDOWN:
- prompt user (with syntax) for input
- save input
- prompt user for operation (product or sum)
- perform calculation
- return string wtih operation result
  - one string is for sum
  - one string is for product

EXAMPLES:
>> Please enter an integer greater than 0:
5
>> Enter 's' to compute the sum, 'p' to compute the product.
s
The sum of the integers between 1 and 5 is 15.


>> Please enter an integer greater than 0:
6
>> Enter 's' to compute the sum, 'p' to compute the product.
p
The product of the integers between 1 and 6 is 720.

------------------------------------------------------------------------------

APPROACH:
- define a method (`numbers`) 
- prompt user (with specific syntax) for positive integer
- initialize local variable to return value from user (`int`) 
  - convert user input from string to integer
- prompt user for type of operation
  - initialize local variable (`operation`) for return value of user input
- create an array from range of 1 upto and including `int` (`tmp_arr`) 
- perform operation on array
  - if operation == 's'
    - sum of all array elements
  - otherwise product of all array elements
    - for each element in array multiply current value with next value and increment to results
- initialize local variable from return value of operation (`operation_value`) 
  - sum or product operation on `tmp_arr` 

TEST CASES:
iterate through each element of array, and it's neighbor

PSEUDOCODE:
def prompt
  return output syntax
end

def numbers
  prompt user input
=end

def prompt(msg)
  puts ">> #{msg}"
end

def numbers
  prompt "Please enter an integer greater than 0:"
  int = gets.chomp.to_i
  prompt "Enter 's' to compute the sum, 'p' to compute the product."
  operation = gets.chomp
  if operation == 's'
    tmp_arr = (1..int).to_a
    tmp_arr.sum
  else
    results = 1
    1.upto(int) {|num| results *= num}
    results
  end
end

p numbers
