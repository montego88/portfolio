=begin
------------------------------------------------------------------------------
Print all even numbers from `1` to `99`, inclusive, to the console, with each
number on a separate line.

BREAKDOWN:
- print every even number in a range

EXAMPLES:

------------------------------------------------------------------------------

APPROACH:
- create a method that takes 2 integers
- output each even number on separate liine

TEST CASES:

PSEUDOCODE:
def evens(x, y)
  iterate starting from x upto and including `y`
  output only even numbers on separte line
end

=end
def evens(x, y)
  x.upto(y) {|num| puts num if num.even?}
end

evens(1, 99)
