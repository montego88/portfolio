# README

This is a repo to use for my assignments and exercises through the RB101 and RB109 modules of Launch School's Core Curriculum.

Note that RB101 and RB109 are focused on beginning Programming Foundations and as such, do not cover Object Oriented Programming. OOP is coming up in my next module of which I am very excited : ).
